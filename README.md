# Supercompany URL Scheme
Easily define URL Scheme and application scheme queries for Android and iOS apps + check if an app is installed via URL availability check.


## Dependencies
- [Easy Project Settings](https://github.com/gilzoide/unity-easy-project-settings)


## Installing
Install this package via [Unity Package Manager](https://docs.unity3d.com/Manual/upm-ui-giturl.html) using the following URL:
```
https://gitlab.com/super-company-git/supercompany-url-scheme.git?path=/Packages/io.supercompany.url-scheme#1.0.1
```


## How to use
1. Open the Project Settings window in the `Supercompany -> URL Settings`
   ![Project Settings window with the "Supercompany -> URL Settings" section selected](Extras~/project-settings.png)
2. Add the schemes you want the application to respond to in the `Own Url Schemes` list
3. Add the schemes you want to query in the `Application Queries Schemes` list.
   From [Android 11](https://developer.android.com/training/package-visibility) and [iOS 9](https://developer.apple.com/documentation/uikit/uiapplication/1622952-canopenurl) and above, listing the URLs you want to query is mandatory.
4. Call `UrlQuery.CanOpenUrl(url)` to check if a URL is supported, that is, if there is an app installed that can open it.
5. Enjoy 🍾
