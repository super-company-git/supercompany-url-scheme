using Supercompany.UrlScheme;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class UrlQuerySample : MonoBehaviour
{
    [SerializeField] private TMP_InputField _inputField;
    [SerializeField] private UnityEvent _canOpenUrlEvent;
    [SerializeField] private UnityEvent _cannotOpenUrlEvent;

    void Start()
    {
        _cannotOpenUrlEvent.Invoke();
    }

    public void CheckCanOpenUrl(string url)
    {
        if (UrlQuery.CanOpenUrl(url))
        {
            _canOpenUrlEvent.Invoke();
        }
        else
        {
            _cannotOpenUrlEvent.Invoke();
        }
    }

    public void OpenUrl()
    {
        string url = _inputField.text;
        if (UrlQuery.CanOpenUrl(url))
        {
            Application.OpenURL(UrlScheme.EnsureUrl(url));
        }
    }
}
