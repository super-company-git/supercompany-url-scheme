#if UNITY_ANDROID
using UnityEngine;

namespace Supercompany.UrlScheme
{
    public static partial class UrlQuery
    {
        private static bool CanOpenUrlAndroid(string url)
        {
#if !UNITY_EDITOR
            using (var unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            using (var context = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity"))
            using (var urlSchemeAndroidClass = new AndroidJavaClass("io.supercompany.urlscheme.UrlSchemeAndroid"))
            {
                return urlSchemeAndroidClass.CallStatic<bool>("canOpenUrl", context, url);
            }
#else
            return false;
#endif
        }
    }
}
#endif