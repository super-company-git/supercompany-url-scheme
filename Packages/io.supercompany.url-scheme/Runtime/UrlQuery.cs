using System;

namespace Supercompany.UrlScheme
{
    public static partial class UrlQuery
    {
        /// <summary>Check if the system can open the given URL or URL scheme.</summary>
        /// <remarks>This is only implemented for the Android and iOS platforms, all other platforms return <see langword="false"/>.</remarks>
        public static bool CanOpenUrl(string urlOrScheme)
        {
            if (string.IsNullOrWhiteSpace(urlOrScheme))
            {
                return false;
            }

#if UNITY_IOS
            return CanOpenUrlIos(UrlScheme.EnsureUrl(urlOrScheme));
#elif UNITY_ANDROID
            return CanOpenUrlAndroid(UrlScheme.EnsureUrl(urlOrScheme));
#else
            return false;
#endif
        }

        /// <summary>Check if the system can open the given URI.</summary>
        /// <remarks>This is only implemented for the Android and iOS platforms, all other platforms return <see langword="false"/>.</remarks>
        public static bool CanOpenUrl(Uri uri)
        {
            return CanOpenUrl(uri.ToString());
        }
    }
}
