#if UNITY_IOS
using System.Runtime.InteropServices;

namespace Supercompany.UrlScheme
{
    public static partial class UrlQuery
    {
        [DllImport("__Internal", CharSet = CharSet.Ansi)]
        public static extern bool UrlSchemeIos_CanOpenUrl(string url);

        private static bool CanOpenUrlIos(string url)
        {
#if !UNITY_EDITOR
            return UrlSchemeIos_CanOpenUrl(url);
#else
            return false;
#endif
        }
    }
}
#endif