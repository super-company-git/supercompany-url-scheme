using System;
using System.Text.RegularExpressions;

namespace Supercompany.UrlScheme
{
    public static class UrlScheme
    {
        /// <summary>Regex that matches URL scheme names.</summary>
        public static readonly Lazy<Regex> SchemePattern = new Lazy<Regex>(() => new Regex(@"[a-zA-Z][-+.a-zA-Z0-9]*"));

        /// <summary>Check if <paramref name="name"/> is a valid URL scheme name.</summary>
        public static bool IsValidSchemeName(string name)
        {
            return Uri.CheckSchemeName(name);
        }

        /// <summary>Ensure that <paramref name="urlOrScheme"/> has a colon character.</summary>
        /// <remarks>
        /// Use this to get a valid URL even if the passed value contains only the URL scheme.
        /// This method doesn't do any more validation than ensuring there is a colon character present.
        /// </remarks>
        public static string EnsureUrl(string urlOrScheme)
        {
            return urlOrScheme.Contains(':') ? urlOrScheme : urlOrScheme + ':';
        }
    }
}
