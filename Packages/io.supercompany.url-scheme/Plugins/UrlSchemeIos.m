#import <UIKit/UIKit.h>

bool UrlSchemeIos_CanOpenUrl(const char *urlString) {
    NSURL *url = [NSURL URLWithString:[NSString stringWithUTF8String:urlString]];
    return [UIApplication.sharedApplication canOpenURL:url];
}
