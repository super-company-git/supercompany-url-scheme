#if UNITY_ANDROID
using System.Collections.Generic;
using System.Xml;
using Gilzoide.EasyProjectSettings;
using UnityEditor.Android;
using UnityEngine;

namespace Supercompany.UrlScheme.Editor
{
    public class AndroidBuildProcessor : IPostGenerateGradleAndroidProject
    {
        public const string AndroidXmlNamespaceUri = "http://schemas.android.com/apk/res/android";

        public int callbackOrder => 0;

        public void OnPostGenerateGradleAndroidProject(string path)
        {
            if (!ProjectSettings.TryLoad(out UrlSchemeSettings settings))
            {
                return;
            }

            var manifestPath = $"{path}/src/main/AndroidManifest.xml";
            var document = new XmlDocument();
            document.Load(manifestPath);

            bool haveChanges = false;
            if (settings.OwnUrlSchemes != null && settings.OwnUrlSchemes.Count > 0)
            {
                bool addedSchemes = AddSchemesToManifestXml(document, settings.OwnUrlSchemes);
                haveChanges = haveChanges || addedSchemes;
                Debug.Log($"[UrlScheme] Added URL schemes: {string.Join(", ", settings.OwnUrlSchemes)}");
            }
            if (settings.ApplicationQueriesSchemes != null && settings.ApplicationQueriesSchemes.Count > 0)
            {
                bool addedSchemes = AddQueriesToManifestXml(document, settings.ApplicationQueriesSchemes);
                haveChanges = haveChanges || addedSchemes;
                Debug.Log($"[UrlScheme] Added URL scheme queries: {string.Join(", ", settings.ApplicationQueriesSchemes)}");
            }

            if (haveChanges)
            {
                document.Save(manifestPath);
            }
        }

        public static bool AddSchemesToManifestXml(XmlDocument document, IEnumerable<string> schemes)
        {
            var namespaceManager = new XmlNamespaceManager(document.NameTable);
            namespaceManager.AddNamespace("android", AndroidXmlNamespaceUri);

            var mainIntentFilterNode = document.SelectSingleNode("/manifest/application/activity/intent-filter/action[@android:name='android.intent.action.MAIN']", namespaceManager);
            var activityNode = mainIntentFilterNode.ParentNode.ParentNode;

            bool addedSchemes = false;
            foreach (string scheme in schemes)
            {
                if (string.IsNullOrEmpty(scheme) || activityNode.SelectSingleNode($"intent-filter/data[@android:scheme='{scheme}']", namespaceManager) != null)
                {
                    continue;
                }

                var newIntentFilter = AddChildElement(activityNode, "intent-filter");
                AddChildElement(newIntentFilter, "action", AndroidXmlNamespaceUri, "name", "android.intent.action.VIEW");
                AddChildElement(newIntentFilter, "category", AndroidXmlNamespaceUri, "name", "android.intent.category.DEFAULT");
                AddChildElement(newIntentFilter, "category", AndroidXmlNamespaceUri, "name", "android.intent.category.BROWSABLE");
                AddChildElement(newIntentFilter, "data", AndroidXmlNamespaceUri, "scheme", scheme);
                addedSchemes = true;
            }
            return addedSchemes;
        }

        public static bool AddQueriesToManifestXml(XmlDocument document, IEnumerable<string> schemes)
        {
            var namespaceManager = new XmlNamespaceManager(document.NameTable);
            namespaceManager.AddNamespace("android", AndroidXmlNamespaceUri);

            var manifestNode = document.SelectSingleNode("/manifest");
            var queriesNode = manifestNode.SelectSingleNode("queries") ?? AddChildElement(manifestNode, "queries");

            bool addedSchemes = false;
            foreach (string scheme in schemes)
            {
                if (string.IsNullOrEmpty(scheme) || queriesNode.SelectSingleNode($"intent/data[@android:scheme='{scheme}']", namespaceManager) != null)
                {
                    continue;
                }

                var newIntentQuery = AddChildElement(queriesNode, "intent");
                AddChildElement(newIntentQuery, "action", AndroidXmlNamespaceUri, "name", "android.intent.action.VIEW");
                AddChildElement(newIntentQuery, "data", AndroidXmlNamespaceUri, "scheme", scheme);
                addedSchemes = true;
            }
            return addedSchemes;
        }

        private static XmlElement AddChildElement(XmlNode sourceElement, string tag)
        {
            var newElement = sourceElement.OwnerDocument.CreateElement(tag);
            sourceElement.AppendChild(newElement);
            return newElement;
        }

        private static XmlElement AddChildElement(XmlNode sourceElement, string tag, string attributeNamespaceUri, string attributeLocalName, string attributeValue)
        {
            var newElement = sourceElement.OwnerDocument.CreateElement(tag);
            newElement.SetAttribute(attributeLocalName, attributeNamespaceUri, attributeValue);
            sourceElement.AppendChild(newElement);
            return newElement;
        }
    }
}
#endif