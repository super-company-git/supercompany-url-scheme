using System.Collections.Generic;
using System.Text.RegularExpressions;
using Gilzoide.EasyProjectSettings;
using UnityEngine;

namespace Supercompany.UrlScheme.Editor
{
    [ProjectSettings("ProjectSettings/UrlSchemeSettings.asset",
        SettingsPath = "Project/Supercompany/URL Settings")]
    public class UrlSchemeSettings : ScriptableObject
    {
        [Tooltip("The URL schemes that the app will declare.\n"
            + "These will be injected to the AndroidManifest.xml for Android apps and in the iOS Project Settings for iOS.")]
        [SerializeField] protected List<string> _ownUrlSchemes;

        [Tooltip("Define here the URL Schemes of the apps that your app will want to check are installed in the device.\n"
            + "This will be injected to the <queries> section of the AndroidManifest.xml and to the LSApplicationQueriesSchemes list in Info.plist for iOS.")]
        [SerializeField] protected List<string> _applicationQueriesSchemes;

        public List<string> OwnUrlSchemes => _ownUrlSchemes;
        public List<string> ApplicationQueriesSchemes => _applicationQueriesSchemes;

        void OnValidate()
        {
            for (int i = 0; i < _ownUrlSchemes.Count; i++)
            {
                _ownUrlSchemes[i] = ValidateSchemeCharacters(_ownUrlSchemes[i]);
            }
            for (int i = 0; i < _applicationQueriesSchemes.Count; i++)
            {
                _applicationQueriesSchemes[i] = ValidateSchemeCharacters(_applicationQueriesSchemes[i]);
            }
        }

        string ValidateSchemeCharacters(string scheme)
        {
            Match m = UrlScheme.SchemePattern.Value.Match(scheme);
            return m.Success ? m.Value : "";
        }
    }
}
