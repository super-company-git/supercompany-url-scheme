#if UNITY_IOS
using System.Collections.Generic;
using System.Linq;
using Gilzoide.EasyProjectSettings;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.iOS.Xcode;

namespace Supercompany.UrlScheme.Editor
{
    public partial class IosBuildProcessor : IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
        public int callbackOrder => 0;

        public void OnPreprocessBuild(BuildReport report)
        {
            if (report.summary.platformGroup != BuildTargetGroup.iOS
                || !ProjectSettings.TryLoad(out UrlSchemeSettings settings)
                || settings.OwnUrlSchemes == null || settings.OwnUrlSchemes.Count == 0)
            {
                return;
            }

            AddIosUrlSchemes(settings.OwnUrlSchemes);
        }

        public void OnPostprocessBuild(BuildReport report)
        {
            if (report.summary.platformGroup != BuildTargetGroup.iOS
                || !ProjectSettings.TryLoad(out UrlSchemeSettings settings)
                || settings.ApplicationQueriesSchemes == null || settings.ApplicationQueriesSchemes.Count == 0)
            {
                return;
            }

            AddApplicationQueriesSchemes(report.summary.outputPath, settings.ApplicationQueriesSchemes);
        }

        public static void AddIosUrlSchemes(IEnumerable<string> schemes)
        {
            string[] currentSchemes = PlayerSettings.iOS.iOSUrlSchemes;
            PlayerSettings.iOS.iOSUrlSchemes = currentSchemes.Union(schemes).ToArray();
        }

        public static void AddApplicationQueriesSchemes(string buildPath, IEnumerable<string> schemes)
        {
            var infoPlistPath = $"{buildPath}/Info.plist";
            var plist = new PlistDocument();
            plist.ReadFromFile(infoPlistPath);

            PlistElementArray applicationQueriesSchemes = (PlistElementArray) plist.root["LSApplicationQueriesSchemes"] ?? plist.root.CreateArray("LSApplicationQueriesSchemes");
            IEnumerable<string> alreadyPresentSchemes = applicationQueriesSchemes.values.Select(element => element.AsString());
            foreach (string scheme in schemes.Except(alreadyPresentSchemes))
            {
                applicationQueriesSchemes.AddString(scheme);
            }

            plist.WriteToFile(infoPlistPath);
        }
    }
}
#endif